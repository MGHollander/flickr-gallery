'use strict';

var gulp = require('gulp');
var concat = require('gulp-concat');
var cssmin = require('gulp-cssmin');
var uglify = require('gulp-uglify');

gulp.task('default', function() {
  // place code for your default task here
});

gulp.task('concat-js', function() {
  return gulp.src([
      './src/vendor/lightgallery/js/lightgallery.min.js',
      './src/vendor/lightgallery/js/lg-*.min.js',
      './src/vendor/blazy.min.js',
      './src/js/utility.js',
      './src/js/flickr-gallery.js'
    ])
    .pipe(concat('flickr-gallery.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist/js/'));
});

gulp.task('concat-css', function() {
  return gulp.src([
      './src/vendor/lightgallery/css/lightgallery.min.css',
      './src/vendor/lightgallery/css/lg-*.min.css',
      './src/css/flickr-gallery.css'
    ])
    .pipe(concat('flickr-gallery.min.css'))
    .pipe(cssmin())
    .pipe(gulp.dest('./dist/css/'));
});

gulp.task('concat', ['concat-css', 'concat-js']);
