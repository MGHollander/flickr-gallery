(function (document, window) {
  'use strict';

  var defaultParameters = {
    container: null,
    callback: 'showPhotos',
    flickr: {
      api_key: null,
      method: 'flickr.photos.search',
      format: 'json',
      nojsoncallback: 1,
      per_page: 500 // Max allowed by the Flickr API
    },
    lightgallery: {
      thumbnail: true
    },
    pager: {
      pagesToShow: 3,
      url: '/page/'
    },
    bLazy : {
      selector: '.flickr-gallery-img',
      successClass: 'flickr-gallery-img-loaded'
    }
  };

  function sendRequest(parameters) {
    var xhr = new XMLHttpRequest();
    var requestParameters = Utility.extend(true, defaultParameters, parameters);

    xhr.open('GET', Utility.buildUrl('https://api.flickr.com/services/rest/', requestParameters.flickr));
    xhr.onload = function() {
      if (xhr.status === 200) {
        var response = JSON.parse(xhr.response);

        if (response.stat != 'ok') {
          console.error(response);
          return;
        }
        
        var fn = FlickrGallery[requestParameters.callback];
        if(typeof fn != 'function') {
          console.error('Callback function `'+ requestParameters.callback +'` does not exist!');
          return;
        }

        fn(document.querySelector(requestParameters.selector), response, requestParameters);
      }
    };
    xhr.send();
  }

  function buildThumbnailUrl(photo) {
    return 'https://farm' + photo.farm + '.staticflickr.com/' + photo.server + '/' + photo.id + '_' + photo.secret + '_q.jpg';
  }

  function buildPhotoUrl(photo) {
    return 'https://farm' + photo.farm + '.staticflickr.com/' + photo.server + '/' + photo.id + '_' + photo.secret + '.jpg';
  }

  function buildPhotoLargeUrl(photo) {
    return 'https://farm' + photo.farm + '.staticflickr.com/' + photo.server + '/' + photo.id + '_' + photo.secret + '_b.jpg';
  }

  function getPhotos(parameters, page) {
    parameters.flickr.page = page > 0 ? page : 1;

    sendRequest(parameters);
  }

  function createPager(element, parameters) {
    var listItem, link;

    var currentPage = parseInt(parameters.currentPage);
    var totalPages = parseInt(parameters.totalPages);
    var pagesToShow = parseInt(parameters.pagesToShow);
    var url = parameters.url;

    element.textContent = '';

    // Previous page button
    var buttonPrev = document.createElement('li');
    buttonPrev.className = 'flickr-gallery-page-prev';
    if (currentPage == 1) {
      buttonPrev.className += ' flickr-gallery-page-number-disabled';
      buttonPrev.innerHTML = '&lt;';
    } else {
      link = document.createElement('a');
      link.href = url + currentPage - 1;
      link.className = 'flickr-gallery-page-number';
      link.setAttribute('data-page-number', (currentPage - 1));
      link.innerHTML = '&lt;';

      buttonPrev.appendChild(link);
    }
    element.appendChild(buttonPrev);

    // Next page button
    var buttonNext = document.createElement('li');
    buttonNext.className = 'flickr-gallery-page-next';

    if (currentPage == totalPages) {
      buttonNext.className += ' flickr-gallery-page-number-disabled';
      buttonNext.innerHTML = '&gt;';
    } else {
      link = document.createElement('a');
      link.href = url + currentPage + 1;
      link.className = 'flickr-gallery-page-number';
      link.setAttribute('data-page-number', (currentPage + 1));
      link.innerHTML = '&gt;';

      buttonNext.appendChild(link);
    }
    element.appendChild(buttonNext);

    // Previous button
    listItem = document.createElement('li');
    if (currentPage == 1) {
      listItem.className = 'flickr-gallery-page-number-disabled';
      listItem.innerHTML = 1;
    } else {
      link = document.createElement('a');
      link.href = url + currentPage - 1;
      link.className = 'flickr-gallery-page-number';
      link.setAttribute('data-page-number', 1);
      link.innerHTML = '<';

      listItem.appendChild(link);
    }

    // Always show the first page
    listItem = document.createElement('li');
    if (currentPage == 1) {
      listItem.className = 'flickr-gallery-page-number-current';
      listItem.innerHTML = 1;
    } else {
      link = document.createElement('a');
      link.href = url + 1;
      link.className = 'flickr-gallery-page-number';
      link.setAttribute('data-page-number', 1);
      link.innerHTML = 1;

      listItem.appendChild(link);
    }

    element.appendChild(listItem);

    // Show dots between the first page and current pager if there are more then 2 number in between
    if (currentPage > (pagesToShow + 3)) {
      listItem = document.createElement('li');
      listItem.innerHTML = '...';
      element.appendChild(listItem);
    }

    // Show the second page instead of dots if there is only one page hidden by the dots
    if (currentPage == (pagesToShow + 3)) {
      link = document.createElement('a');
      link.href = url + 2;
      link.className = 'flickr-gallery-page-number';
      link.setAttribute('data-page-number', 2);
      link.innerHTML = 2;

      listItem = document.createElement('li');
      listItem.appendChild(link);

      element.appendChild(listItem);
    }

    var startIndex = currentPage - pagesToShow;
    var endIndex = currentPage + pagesToShow + 1;

    for (var i = startIndex; i < endIndex; i++) {
      if (i <= 1 || i >= totalPages) {
        continue;
      }

      listItem = document.createElement('li');
      if (i === currentPage) {
        listItem.className = 'flickr-gallery-page-number-current';
        listItem.innerHTML = i;
      } else {
        link = document.createElement('a');
        link.href = url + i;
        link.className = 'flickr-gallery-page-number';
        link.setAttribute('data-page-number', i);
        link.innerHTML = i;

        listItem.appendChild(link);
      }

      element.appendChild(listItem);
    }

    // Show dots between the last page and current pager if there are more then 2 number in between
    if (currentPage < (totalPages - pagesToShow - 2)) {
      listItem = document.createElement('li');
      listItem.innerHTML = '...';
      element.appendChild(listItem);
    }

    // Show the page before the last instead of dots if there is only one page hidden by the dots
    if (currentPage == (totalPages - pagesToShow - 2)) {
      link = document.createElement('a');
      link.href = url + (totalPages - 1);
      link.className = 'flickr-gallery-page-number';
      link.setAttribute('data-page-number', totalPages - 1);
      link.innerHTML = totalPages - 1;

      listItem = document.createElement('li');
      listItem.appendChild(link);

      element.appendChild(listItem);
    }

    // Always show the last page
    listItem = document.createElement('li');
    if (currentPage == totalPages) {
      listItem.className = 'flickr-gallery-page-number-current';
      listItem.innerHTML = totalPages;
    } else {
      link = document.createElement('a');
      link.href = url + totalPages;
      link.className = 'flickr-gallery-page-number';
      link.setAttribute('data-page-number', totalPages);
      link.innerHTML = totalPages;

      listItem.appendChild(link);
    }

    element.appendChild(listItem);
}

  function showPhoto(container, data, parameters) {
    container.textContent = '';
    var image, link;

    image = document.createElement('img');
    image.src = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
    image.setAttribute('data-src', buildPhotoLargeUrl(data.photo));
    image.className = 'flickr-gallery-img';
    image.alt = data.photo.title._content;
    image.title = data.photo.title._content;

    link = document.createElement('a');
    link.href = buildPhotoLargeUrl(data.photo);
    link.className = 'flickr-gallery-img-link';
    link.appendChild(image);

    container.appendChild(link);

    var galleryParameters = Utility.extend(true, defaultParameters, parameters);

    new Blazy(galleryParameters.bLazy);
    lightGallery(container, galleryParameters.lightgallery);
  }
  
  function showPhotos(container, data, parameters) {
    if (data.photos.pages > 1) {
      createPager(
        container.getElementsByClassName('flickr-gallery-pager')[0], {
          currentPage: data.photos.page,
          totalPages: data.photos.pages
        }
      );
    }

    FlickrGallery.createGallery(container.getElementsByClassName('flickr-gallery')[0], data.photos.photo, parameters);
  }

  function showPhotoSet(container, data, parameters) {
    if (data.photoset.pages > 1) {
      createPager(
        container.getElementsByClassName('flickr-gallery-pager')[0], {
          currentPage: data.photoset.page,
          totalPages: data.photoset.pages,
          pagesToShow: parameters.pager.pagesToShow,
          url: parameters.pager.url
        }
      );
    }

    FlickrGallery.createGallery(container.getElementsByClassName('flickr-gallery')[0], data.photoset.photo, parameters);
  }

  function createGallery(container, photos, parameters) {
    container.textContent = '';
    var image, link;
    for (var i = 0; i < photos.length; i++) {
      image = document.createElement('img');
      image.src = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
      image.setAttribute('data-src', buildThumbnailUrl(photos[i]));
      image.className = 'flickr-gallery-img';
      image.alt = photos[i].title;
      image.title = photos[i].title;

      link = document.createElement('a');
      link.href = buildPhotoLargeUrl(photos[i]);
      link.className = 'flickr-gallery-img-link';
      link.appendChild(image);

      container.appendChild(link);
    }

    var galleryParameters = Utility.extend(true, defaultParameters, parameters);

    new Blazy(galleryParameters.bLazy);
    lightGallery(container, galleryParameters.lightgallery);
  }

  function init(parameters) {
    if (!parameters.selector) {
      console.error('No selector defined!');
      return;
    }

    if (!parameters.flickr.api_key) {
      console.error('No Flickr API key defined!');
      return;
    }

    var container = document.querySelector(parameters.selector);
      
    if (container.getElementsByClassName('flickr-gallery-pager').length > 0) {
      container.getElementsByClassName('flickr-gallery-pager')[0].addEventListener('click', function (event) {
        event.preventDefault();

        var page = event.target.getAttribute('data-page-number');

        // Avoid reloading the same page
        if (page) {
          getPhotos(parameters, page);
        }
      });
    }

    // Kickstart the page
    getPhotos(parameters, 1);
  }

  window.FlickrGallery = Utility.extend(window.FlickrGallery || {}, {
    init: init,
    showPhoto: showPhoto,
    showPhotos: showPhotos,
    showPhotoSet: showPhotoSet,
    createGallery: createGallery
  });
})(document, window);
